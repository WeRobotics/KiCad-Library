# KiCad library


1. Get the central lib onto your computer (git clone, svn checkout, mount network drive)
2. Setup path variables (in KiCad) pointing to this storage location as required (at least for 3d models)
3. Use the library managers (footprint and symbol) to add these libraries to your setup (to the global library table)
