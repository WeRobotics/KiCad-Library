# Downloaded components

If a component is not in the standard KiCad libraries and not in one of the libraries included as submodule download the component and add it here. 

Sources to download:
- (PREFERRED) https://www.ultralibrarian.com/
- https://octopart.com/
- https://www.snapeda.com/

Please add download source in the table below.

|Component|Download page|Date|
|---|---|---|
|PCA9615|https://app.ultralibrarian.com/details/ca0cfa61-a7fd-11e9-ab3a-0a3560a4cccc/NXP/PCA9615DPJ?uid=27bbeca4a0f6f87c|2021-05-21|
|lcw+mvsg.ec|https://octopart.com/lcw+mvsg.ec-bxcx-4l8n-1-osram+opto-51128826?r=sp|2021-05-27|
|  |   |
